<?php

namespace KlezApi\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\App;
use Cake\Core\Configure;
use KlezApi\Plugin;

/**
 * This Command generates a config entry, into the endpoints config file, for a Bare Endpoint.
 *
 * Class GeneratorCommand
 * @package KlezApi\Command
 */
class GeneratorCommand extends Command {
    /**
     * @param ConsoleOptionParser $parser
     * @return ConsoleOptionParser
     */
    protected function buildOptionParser(ConsoleOptionParser $parser){
        $parser->addArgument('endpoint', [
            'help' => 'Endpoint\'s name to generate'
        ]);

        return $parser;
    }

    /**
     * Command execution entry point.
     *
     * Firstly, sanitizes the command input, a endpoint name is required.
     * Afterwards it tries to load the endpoint config file, and check if the endpoint is already there, in such case
     * the execution is halted.
     * Finally, it appends the new bare entry into the endpoint config file.
     *
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|void|null
     */
    public function execute(Arguments $args, ConsoleIo $io){
        $endpoint = $args->getArgument('endpoint');

        if(empty($endpoint)){
            $io->error('Endpoint\'s name is required');
            $this->abort(2);
        }

        $file = $this->resolveEndpointsFile();
        $content = require $file;
        $key = 'KlezApi.endpoints.' . $endpoint;

        if(isset($content[$key])){
            $io->error('Endpoint\'s already configured');
            $this->abort(2);
        }

        $content[$key] = [
            'config' => [

            ],
            'pipeline' => [
                App::className('KlezApi.Formatter','Controller/Node', 'Node'),
            ]
        ];

        $newContent = $this->generateNewContent($content);
        file_put_contents($file,$newContent);
    }

    /**
     * Resolvs the endpoints file path.
     *
     * It leverages the Plugin class load flow, in this loading process the endpoints file name is stored in the
     * framework's configuration.
     *
     * @see Plugin
     * @return string The endpoint config file path.
     */
    private function resolveEndpointsFile(){
        $file = Configure::read(Plugin::CONFIG_ENDPOINTS);
        return CONFIG . $file . '.php';
    }

    /**
     * Translates the php array into its php code equivalent.
     *
     * @param array $content The new endpoint config as a standalone php assoc array.
     * @return string The php code equivalent to the php assoc array provided.
     */
    private function generateNewContent($content){
        $newContent = "<?php\n\n";
        $newContent .= "use Cake\Core\App;\n\n";
        $newContent .= "return [\n";

        foreach($content as $key => $value){
            $newContent .= $this->phpArrayGenerator($key,$value);
        }

        $newContent .= "];\n";

        return $newContent;
    }

    /**
     * Translates a php key/value pair into its php code equivalent.
     *
     * It supports array values applying recursion. Tries to generate pretty code with tabs and breaks when needed.
     *
     * @param $key
     * @param $value
     * @param int $level
     * @return string
     */
    private function phpArrayGenerator($key, $value, $level = 1){
        $encodeKey = json_encode($key);
        $newContent = str_repeat("\t", $level) . "{$encodeKey} => ";

        if(is_array($value)){
            $newContent .= "[\n";

            foreach($value as $k => $v){
                $newContent .= $this->phpArrayGenerator($k,$v,$level + 1);
            }

            $newContent .= str_repeat("\t", $level) . "],\n";
        }
        else{
            $encode = json_encode($value, JSON_UNESCAPED_SLASHES);
            $newContent .= "{$encode},\n";
        }

        return $newContent;
    }
}