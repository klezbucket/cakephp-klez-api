<?php

namespace KlezApi;

use Cake\Core\BasePlugin;
use Cake\Core\Configure;
use Cake\Log\Log;
use KlezCore\Log\Engine\KlezLog;

/**
 * Class Plugin
 * @package KlezApi
 */
class Plugin extends BasePlugin{
    /**
     * Default for the maximum message length written.
     */
    const LOGGING_MAX_LENGTH = 255;
    /**
     * Default logging file name.
     */
    const LOGGING_FILE = 'klezapi';
    /**
     *  Default logging scopes.
     */
    const LOGGING_SCOPES = [ 'klezapi' ];
    /**
     *  Default logging levels.
     */
    const LOGGING_LEVELS = [];
    /**
     *  Configure entry for the endpoint file name.
     */
    const CONFIG_ENDPOINTS = 'KlezApi.config.endpoints';
    /**
     *  Configure entry for the logging settings.
     */
    const CONFIG_LOGGING = 'KlezApi.config.logging';

    /**
     * Plugin constructor.
     * @param array $options
     */
    public function __construct(array $options = []){
        parent::__construct($options);

        $this->loadConfig($options);
        $this->loadLogging($options);
    }

    /**
     * @param array $options
     */

    private function loadConfig(array $options){
        $this->loadEndpointsConfig($options);
    }

    /**
     * Load the endpoint config file.
     *
     * The key config entry is 'endpoints'. It denotes the config file name relative to the /config project folder.
     * Writes the endpoint file name into the framework configuration.
     *
     * @param array $options
     */

    private function loadEndpointsConfig(array $options){
        if(isset($options['config']['endpoints'])){
            Configure::load($options['config']['endpoints']);
            Configure::write(self::CONFIG_ENDPOINTS, $options['config']['endpoints']);
        }
    }

    /**
     * Load the logging to file engine.
     *
     * The log.enabled flag determines the engine boot, no logging is established by default.
     * The log.file string determines the file name to log to, self::LOGGING_FILE resolves by default.
     * The log.scopes array is defaulted to self::LOGGING_SCOPES if none specified.
     * The log.levels array is defaulted to self::LOGGING_LEVELS if none specified.
     * Other config entries are passed directly to the config adapter.
     * Writes the logging settings into the framework configuration.
     *
     * @see KlezLog
     * @param array $options
     */

    private function loadLogging(array $options){
        $options['log']['file'] = $options['log']['file'] ?? self::LOGGING_FILE;
        $options['log']['scopes'] = $options['log']['scopes'] ?? self::LOGGING_SCOPES;
        $options['log']['levels'] = $options['log']['levels'] ?? self::LOGGING_LEVELS;
        $options['log']['maxlength'] = $options['log']['maxlength'] ?? self::LOGGING_MAX_LENGTH;
        $options['log']['className'] = 'KlezCore.Klez';
        Log::setConfig($options['log']['file'], $options['log']);
        Configure::write(self::CONFIG_LOGGING, $options['log']);
    }
}
