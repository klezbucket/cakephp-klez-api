<?php

namespace KlezApi\Controller;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Exception;
use KlezApi\Controller\AppController as KlezApiController;
use KlezApi\Controller\Component\LoggingComponent;
use KlezApi\Controller\Component\PipelineComponent;
use KlezApi\Plugin;
use Psr\Log\LogLevel;

/**
 * Plugin's main controller.
 *
 * Class WebserviceController
 * @property PipelineComponent Pipeline
 * @property LoggingComponent Logging
 * @package KlezApi\Controller
 */
class WebserviceController extends KlezApiController {
    /**
     * @param Event $event
     * @return Response|null
     * @throws Exception
     */
    public function beforeFilter(Event $event){
        $this->loggingInit();
        $this->autoRender = false;
        return parent::beforeFilter($event);
    }

    /**
     * @param Event $event
     * @return Response|null
     */
    public function afterFilter(Event $event){
        $this->loggingEnd();
        return parent::afterFilter($event);
    }

    /**
     * Controller's execution entry point.
     *
     * Reads the endpoint routed param and tries to load its config entry or file.
     * Afterwards, hooks the execution to the Pipeline component.
     *
     * @see PipelineComponent
     */
    public function index(){
        $this->Logging->separator();
        $endpoint = $this->request->getParam('endpoint');
        $config = Configure::read('KlezApi.endpoints.' . $endpoint);

        if(empty($config)){
            return $this->notFound();
        }

        if(!is_array($config)){
            $file = CONFIG . $config . '.php';

            if(file_exists($file)){
                Configure::load($config, 'default',false);
                $config = Configure::read('KlezApi.endpoints.' . $endpoint);
            }
        }

        if(!is_array($config)){
            return $this->notFound();
        }

        try{
            $this->loadComponent('KlezApi.Pipeline', $config);
            $this->Pipeline->run();
        }
        catch (Exception $e){
            return $this->exceptionHandler();
        }

        $this->Logging->separator();
    }

    /**
     * Overloads the 'internal error' pipeline when a exception is caught.
     */
    private function exceptionHandler(){
        $this->Pipeline->initialize([
            'pipeline' => [
                App::classname('KlezApi.InternalError','Controller/Node','Node'),
                App::classname('KlezApi.Formatter','Controller/Node','Node'),
            ]
        ]);

        $this->Pipeline->run();
    }

    /**
     * Overloads the 'not found' pipeline when the endpoint entry is not found.
     *
     * @throws Exception
     */
    private function notFound(){
        $this->loadComponent('KlezApi.Pipeline', [
            'pipeline' => [
                App::classname('KlezApi.NotFound','Controller/Node','Node'),
                App::classname('KlezApi.Formatter','Controller/Node','Node'),
            ]
        ]);

        $this->Pipeline->run();
    }

    /**
     * Initializes the Plugin's logging adapter.
     *
     * Logs the request headers and body.
     *
     * @see ServerRequest
     * @throws Exception
     */
    private function loggingInit(){
        $config = Configure::read(Plugin::CONFIG_LOGGING);
        unset($config['className']);

        $method = $this->request->getMethod();
        $this->loadComponent('KlezApi.Logging', $config);
        $this->log("################## {$method} {$this->request->getRequestTarget()} ##################",'info');
        $this->log("ENDPOINT = {$this->request->getParam('endpoint')}",'info');
        $this->log("FORMAT = {$this->request->getParam('format')}",'info');
        $this->Logging->separator();
        $this->log("REQUEST",'info');
        $this->log("\tHEADERS",'info');

        foreach($this->request->getHeaders() as $header => $content){
            $value = $content[0] ?? '';
            $this->log("\t\t{$header}: {$value}",'info');
        }

        $this->log("\tBODY",'info');
        $this->log("\t\t{$this->request->getBody()}",'info');
    }

    /**
     * Logs the response headers and body.
     *
     * @see Response
     */
    private function loggingEnd(){
        $this->log("RESPONSE {$this->response->getStatusCode()}",'info');
        $this->log("\tHEADERS",'info');

        foreach($this->response->getHeaders() as $header => $content){
            $this->log("\t\t{$header}: {$content[0]}",'info');
        }

        $this->log("\tBODY",'info');
        $this->log("\t\t{$this->response->getBody()}",'info');
        $this->Logging->separator();
    }

    /**
     * @param mixed $msg
     * @param int|string $level
     * @param array $context
     * @return bool|void
     */
    public function log($msg, $level = LogLevel::ERROR, $context = []){
        $this->Logging->log($msg,$level,$context);
    }
}