<?php


namespace KlezApi\Controller\Component;


use Cake\Controller\Component;
use Cake\Log\Log;
use KlezApi\Plugin;

/**
 * Logging Component for the KlezApi.
 *
 * Class LoggingComponent
 * @package KlezApi\Controller\Component
 */
class LoggingComponent extends Component {
    /**
     * @var bool Determines if the separator can be printed
     */
    private $separatorAllowed = true;

    /**
     * The scopes used for the Plugin's log.
     *
     * @var array.
     */
    private $scopes = [];

    /**
     * Initializes the component with the Plugin logging settings.
     *
     * @see Plugin
     * @param array $config
     */
    public function initialize(array $config){
        parent::initialize($config);
        $this->scopes = $config['scopes'] ?? [];
    }

    /**
     * @param mixed $msg
     * @param int|string $level
     * @param array $context Unused, scopes is provided instead.
     * @return bool|void
     */
    public function log($msg, $level = LogLevel::ERROR, $context = []){
        Log::write($level,$msg,$this->scopes);
        $this->separatorAllowed = true;
    }

    /**
     * Logs an aesthetic log separator.
     *
     * The separator cannot be printed twice in a row.
     */
    public function separator(){
        if(!$this->separatorAllowed){
            return;
        }

        $this->log("--- --- --- --- ---",'info');
        $this->separatorAllowed = false;
    }
}