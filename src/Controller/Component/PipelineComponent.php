<?php

namespace KlezApi\Controller\Component;
use Cake\Controller\Component;
use Cake\Core\App;
use KlezApi\Controller\Node\FormatterNode;
use KlezApi\Controller\Node\Node;
use KlezApi\Controller\WebserviceController;
use Psr\Log\LogLevel;
use Cake\Core\Configure;

/**
 * Flows the execution through it nodes.
 *
 * Given a endpoint config 'pipeline' entry, iterates through it nodes and executes them in order.
 *
 * It is possible to a certain node to jump across other pipelines defined in the endpoint's config. Each pipeline
 * must have a name denoted by its key in the assoc array . Therefore, calling the jump() method with the desired
 * pipeline's name, derives the flow execution to this alternative pipeline. The flow is run in a recursive fashion,
 * so reaching the final node in an alternative pipeline continues the flow into the next node of the original calling
 * pipeline.
 *
 * Class PipelineComponent
 * @package KlezApi\Controller\Component
 */
class PipelineComponent extends Component {
    /**
     * @var WebserviceController
     */
    private $Controller;
    /**
     * @var array Holds the hardcoded endpoint config.
     */
    private $config = [];
    /**
     * @var array Holds the main pipeline's node set.
     */
    private $pipeline = [];
    /**
     * @var array Holds the alternatives pipeline's node set.
     */
    private $pipelines = [];
    /**
     * @var array Shared memory between nodes. It's also the output for the FormatterNode.
     * @see FormatterNode
     */
    private $buffer = [];
    /**
     * @var bool Interrupts the current pipeline.
     */
    private $halt = false;
    /**
     * @return array
     */
    public function getConfiguration(){
        return $this->config;
    }

    /**
     * @param array $config
     */
    public function initialize(array $config){
        parent::initialize($config);

        if(isset($config['config'])){
            $this->config = $config['config'];
        }

        if(isset($config['pipeline'])){
            $this->pipeline = $config['pipeline'];
        }

        unset($config['pipeline']);
        unset($config['config']);

        $this->pipelines = $config;
        $this->Controller = $this->getController();
    }

    /**
     * Runs the main pipeline.
     *
     * @see Node
     */
    public function run(){
        $pipeline = $this->pipeline;
        $this->runPipeline($pipeline);
    }

    /**
     * Joins the flow into the specified pipeline.
     *
     * If the pipeline doesn't exist, the join is not performed.
     * @param string $name The pipeline's name as denoted by its key into the endpoint's assoc array specification.
     * @haltblock This method cannot run if the current pipeline is halted.
     */

    public function jump($name){
        if($this->halt){
            return;
        }

        $pipeline = $this->pipelines[$name] ?? [];
        $this->runPipeline($pipeline);
    }

    /**
     * Halts the current pipeline.
     *
     * The execution flow continues in the calling pipeline, if any.
     *
     */
    public function halt(){
        $this->halt = true;
    }

    /**
     * Iterates though every node in the pipeline array specified.
     *
     * If the node specified doesn't implement the Node interface it is ignored.
     *
     * A Node can jump across pipelines, see the jump() method.
     *
     * A Pipeline can be halted, see the halt() method.
     *
     * @param array $pipeline The collection of Nodes
     * @see Node
     */
    private function runPipeline(array $pipeline){
        foreach($pipeline as $classname){
            $this->nodeCall($classname);

            if($this->halt){
                $this->halt = false;
                return;
            }
        }
    }

    /**
     * Try to run the individual node class.
     *
     * @param string $classname The Node class to instantiate an run.
     */
     private function nodeCall($classname){
        $node = $this->nodeLoad($classname);

        if(!$node){
            $this->__log();
            $this->log('Node Not Found: ' . $classname, 'info');
            $this->__log();
            return;
        }

        $node->initialize($this);
        $node->run();
     }

     /**
      * Given a Node classname, tries to instantiate it and checks for its superclass.
      * @param string $classname The Node class to instantiate.
      * @return Node|boolean the resulting node or false if its not a valid Node implementation.
      */
      private function nodeLoad($classname){
        if(empty($classname) || !class_exists($classname)){
            return false;
        }

        $superclass = App::className('KlezApi.Node','Controller/Node');
        $node = new $classname;

        if(!$node instanceof $superclass){
            return false;
        }

        return $node;
      }

    /**
     * Concretizes an abstract pipeline called by $name.
     *
     * It looks for the KlezApi.abstracts.$name configuration. Then, the pipeline and config specified are joined into the
     * execution flow. If such configuration doesn't exist nothing happens (treated as an empty pipeline).
     *
     * If the configuration provide alternative pipelines, they are also appended into the pipeline assoc array.
     */

    public function concretize($name){
        $abstract = Configure::read("KlezApi.abstracts.{$name}") ?? [];
        $pipeline = $abstract['pipeline'] ?? [];
        $config = $abstract['config'] ?? [];

        unset($abstract['pipeline']);
        unset($abstract['config']);

        foreach($abstract as $altName => $altPipeline){
            $this->join($altName,$altPipeline);
        }

        $this->join($name,$pipeline,$config);
        $this->jump($name);
    }

    /**
     * Appends a new pipeline into the pipeline's assoc array.
     *
     * It also may append new configuration rules.
     *
     * @param string $name The pipeline's name
     * @param array $pipeline The pipeline's nodes set.
     * @param array $config The pipeline's hardcoded config to append.
     */

    private function join($name,$pipeline = [],$config = []){
        $this->pipelines[$name] = $pipeline;
        $this->config = array_merge($this->config,$config);
    }

    /**
     * Reads from the buffer.
     *
     * @param null $key
     * @return array|mixed|null
     */
    public function read($key = null){
        if(is_null($key)){
            return $this->buffer;
        }

        return $this->buffer[$key] ?? null;
    }

    /**
     * Writes into the buffer.
     *
     * @param $key
     * @param $val
     */
    public function write($key, $val){
        $this->buffer[$key] = $val;
    }

    /**
     * Drop a entry from the buffer.
     *
     * @param $key
     */
    public function delete($key){
        unset($this->buffer[$key]);
    }

    /**
     * Replaces the whole buffer.
     *
     * @param $val
     */
    public function overwrite($val){
        $this->buffer = $val;
    }

    /**
     * Exposes the Logging component to the pipeline.
     *
     * @see LoggingComponent
     * @param mixed $msg
     * @param int|string $level
     * @param array $context
     * @return bool|void
     */
    public function log($msg, $level = LogLevel::ERROR, $context = []){
        $this->Controller->Logging->log($msg,$level,$context);
    }

    /**
     * Exposes the Logging component aesthetic separator.
     */
    public function __log(){
        $this->Controller->Logging->separator();
    }
}
