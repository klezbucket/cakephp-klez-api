<?php

namespace KlezApi\Controller\Node;
use KlezApi\Controller\Node\Node as Node;

/**
 * Output a 404 error.
 *
 * Class NotFoundNode
 * @package KlezApi\Controller\Node
 */
class NotFoundNode extends Node {
    /**
     * @return mixed|void
     */
    function run(){
        $response = $this->response()->withStatus(404);
        $this->response($response);
    }
}