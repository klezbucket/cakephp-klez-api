<?php

namespace KlezApi\Controller\Node;
use Exception;
use KlezApi\Controller\Node\Node as Node;

/**
 * Throws an Exception.
 *
 * Class ExceptionThrowerNode
 * @package KlezApi\Controller\Node
 */
class ExceptionThrowerNode extends Node {
    /**
     * @return mixed|void
     * @throws Exception
     */
    function run(){
        throw new Exception('Im a madman');
    }
}