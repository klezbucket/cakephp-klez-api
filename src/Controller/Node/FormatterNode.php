<?php

namespace KlezApi\Controller\Node;
use KlezApi\Controller\Node\Node as Node;

/**
 * Generates the formatted output, dictated by the format routed param.
 *
 * Class FormatterNode
 * @package KlezApi\Controller\Node
 */
class FormatterNode extends Node {
    /**
     * Executes the function related to the supported format.
     *
     * Each function must generate the serialized buffer and output it to the body. It may also be required to honor
     * the Content-Type header.
     *
     * @return mixed|void
     */
    function run(){
        switch (strtolower($this->request()->getParam('format'))){
            case 'json':
                return $this->json();
            case 'xml':
                return $this->xml();
            case 'php':
                return $this->php();
            case 'yml':
                return $this->yml();
        }
    }

    /**
     * Generates a JSON document
     */
    private function json(){
        $output = json_encode($this->read());
        $response = $this->response()
                            ->withType('application/json')
                            ->withStringBody($output)
                            ->withHeader('Content-Length', strlen($output));

        $this->response($response);
    }

    /**
     * Generates an XML document
     */
    private function xml(){
        $output = xmlrpc_encode($this->read());
        $response = $this->response()
                            ->withType('application/xml')
                            ->withStringBody($output)
                            ->withHeader('Content-Length', strlen($output));

        $this->response($response);

    }

    /**
     * Generates a PHP serialize() string
     */
    private function php(){
        $output = serialize($this->read());
        $response = $this->response()
                            ->withType('application/php')
                            ->withStringBody($output)
                            ->withHeader('Content-Length', strlen($output));

        $this->response($response);
    }

    /**
     * Generates a YAML document
     */
    private function yml(){
        $output = yaml_emit($this->read());
        $response = $this->response()
                            ->withType('application/yaml')
                            ->withStringBody($output)
                            ->withHeader('Content-Length', strlen($output));

        $this->response($response);
    }
}