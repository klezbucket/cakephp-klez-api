<?php

namespace KlezApi\Controller\Node;
use KlezApi\Controller\Node\Node as Node;

/**
 * Picks a random value (1,100) and writes it into the buffer.
 *
 * Class RandomIncrementNode
 * @package KlezApi\Controller\Node
 */
class RandomIncrementNode extends Node {
    /**
     * Buffer's entry.
     */
    const BUFFER_INCREMENT = 'increment';

    /**
     * Puts the randomly picked value into the buffer.
     *
     * @return mixed|void
     */
    function run(){
        $rand = rand(1,100);
        $this->write(self::BUFFER_INCREMENT, $rand);
        $this->info('Increment: ' . $rand);
    }
}