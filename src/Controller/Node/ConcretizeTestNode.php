<?php

namespace KlezApi\Controller\Node;
use KlezApi\Controller\Node\Node as Node;

/**
 * 
 * Loads the pipeline specified by the config.concretize setting, then jumps into it.
 *
 * Class ConcretizeTestNode
 * @package KlezApi\Controller\Node
 * @internal
 */
class ConcretizeTestNode extends Node {
    /**
     * Pipeline to concretize.
     */
    const CONCRETIZE = 'concretize';

    /**
     * Jumps into the pipeline specified by CONCRETIZE.
     *
     * @return mixed|void
     */
    function run(){
        $name = $this->config(self::CONCRETIZE);
        $this->concretize($name);
    }
}