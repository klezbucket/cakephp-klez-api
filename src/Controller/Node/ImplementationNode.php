<?php

namespace KlezApi\Controller\Node;
use KlezApi\Controller\Node\Node as Node;

class ImplementationNode extends Node{
    const IMPLEMENTS = 'implements';

    public function run(){
        $pipeline = $this->config(self::IMPLEMENTS);
        return $this->concretize($pipeline);
    }
}
