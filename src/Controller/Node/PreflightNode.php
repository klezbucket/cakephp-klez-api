<?php

namespace KlezApi\Controller\Node;
use KlezApi\Controller\Node\Node as Node;

/**
 * Checks for the OPTIONS method to serve the CORS' Preflight Headers
 *
 * Class PreflightNode
 * @package KlezApi\Controller\Node
 * @internal
 */
class PreflightNode extends Node {
    const OPTIONS_METHOD = 'options';
    const DEFAULT_HEADERS = [
        'Access-Control-Allow-Origin' => '*',
        'Access-Control-Allow-Methods' => 'GET,POST,DELETE,PUT,OPTIONS,PATCH',
        'Access-Control-Allow-Headers' => 'Accept,Authorization,Content-Type,If-Match,If-None-Match,If-Modified-Since,Expires,ETag,Host,User-Agent',
    ];

    /**
     * Checks for the OPTIONS requests, serves the Preflight Headers configured at config.preflight
     * 
     * If such settings is not specified, default values are passed by.
     * 
     * This node halts the pipeline execution if the OPTIONS method is detected.
     *
     * @return void
     */
    function run(){
        $method = strtolower($this->request()->getMethod());

        if($method === self::OPTIONS_METHOD){
            $this->serveHeaders();
            $this->halt();
        }
    }

    /**
     * Serves the config.preflight specified, if the required headers by spec are not set, default values are
     * passed by.
     *
     * @return void
     */
    private function serveHeaders(){
        $response = $this->response();
        $headers = $this->config('preflight') ?? [];

        foreach(self::DEFAULT_HEADERS as $headerName => $headerValue){
            $headers[$headerName] = $headers[$headerName] ?? $headerValue;
        }

        foreach($headers as  $headerName => $headerValue){
            $response = $response->withHeader($headerName,$headerValue);
        }

        $this->response($response);
    }
}