<?php

namespace KlezApi\Controller\Node;
use KlezApi\Controller\Node\Node as Node;

/**
 * Accumulates the increment value, which resides in the buffer.
 *
 * Class BufferCounterNode
 * @package KlezApi\Controller\Node
 */
class BufferCounterNode extends Node {
    /**
     * Buffer's entry. It denotes the increment value.
     */
    const BUFFER_INCREMENT = 'increment';
    /**
     * The buffer entry where the accumulation proceeds.
     */
    const BUFFER_ENTRY = 'c';

    /**
     * Increments the buffer entry by the increment.
     *
     * @return mixed|void
     */
    function run(){
        $counter = $this->read(self::BUFFER_ENTRY);
        $counter += $this->read(self::BUFFER_INCREMENT);
        $this->write(self::BUFFER_ENTRY, $counter);
        $this->info('Accumulated: ' . $counter);
    }
}