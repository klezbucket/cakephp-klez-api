<?php

namespace KlezApi\Controller\Node;
use KlezApi\Controller\Node\Node as Node;

/**
 * Outputs a 500 error.
 *
 * Class InternalErrorNode
 * @package KlezApi\Controller\Node
 */
class InternalErrorNode extends Node {
    /**
     * @return mixed|void
     */
    function run(){
        $response = $this->response()->withStatus(500);
        $this->response($response);
    }
}