<?php

namespace KlezApi\Controller\Node;
use KlezApi\Controller\Node\Node as Node;

/**
 * Joins the flow execution into the 'pipeline3' pipeline. (hardcoded)
 *
 * Class JumpTest2Node
 * @package KlezApi\Controller\Node
 * @internal
 */
class JumpTest2Node extends Node {
    /**
     * Pipeline to jump into.
     */
    const PIPELINE = 'pipeline3';

    /**
     * Jumps into PIPELINE.
     *
     * @return mixed|void
     */
    function run(){
        $this->jump(self::PIPELINE);
    }
}