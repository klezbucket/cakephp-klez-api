<?php

namespace KlezApi\Controller\Node;
use KlezApi\Controller\Node\Node as Node;

/**
 * Joins the flow execution into the current http method pipeline, denoted by the lowercase method's name.
 *
 * Class MethodJumpNode
 * @package KlezApi\Controller\Node
 * @internal
 */
class JumpMethodNode extends Node {
    /**
     * Jumps into the lowercase http method's name pipeline.
     *
     * @return mixed|void
     */
    function run(){
        $method = strtolower($this->request()->getMethod());
        $this->jump($method);
    }
}