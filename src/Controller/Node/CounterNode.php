<?php

namespace KlezApi\Controller\Node;
use KlezApi\Controller\Node\Node as Node;

/**
 * Accumulates the increment value.
 *
 * Class CounterNode
 * @package KlezApi\Controller\Node
 */
class CounterNode extends Node {
    /**
     * The entry into the config. It denotes the increment value.
     */
    const CONFIG_INCREMENT = 'increment';
    /**
     * The buffer entry where the accumulation proceeds.
     */
    const BUFFER_ENTRY = 'c';

    /**
     * Increments the buffer entry by the increment.
     *
     * @return mixed|void
     */
    function run(){
        $counter = $this->read(self::BUFFER_ENTRY);
        $counter += $this->config(self::CONFIG_INCREMENT);
        $this->write(self::BUFFER_ENTRY, $counter);
        $this->info('Accumulated: ' . $counter);
    }
}