<?php

namespace KlezApi\Controller\Node;
use KlezApi\Controller\Node\Node as Node;

/**
 * Joins the flow execution into the 'pipeline2' pipeline. (hardcoded)
 *
 * Class JumpTestNode
 * @package KlezApi\Controller\Node
 * @internal
 */
class JumpTestNode extends Node {
    /**
     * Pipeline to jump into.
     */
    const PIPELINE = 'pipeline2';

    /**
     * Jumps into PIPELINE.
     *
     * @return mixed|void
     */
    function run(){
        $this->jump(self::PIPELINE);
    }
}