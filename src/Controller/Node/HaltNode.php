<?php

namespace KlezApi\Controller\Node;
use KlezApi\Controller\Node\Node as Node;

/**
 * Halts the pipeline.
 *
 * If this pipeline is actually running from another one, the execution flow returns to the this calling pipeline.
 *
 * Class HaltNode
 * @package KlezApi\Controller\Node
 * @internal
 */
class HaltNode extends Node {
    /**
     * Halts the current pipeline.
     *
     * @return mixed|void
     */
    function run(){
        $this->halt();
    }
}