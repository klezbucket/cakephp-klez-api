<?php

namespace KlezApi\Controller\Node;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use KlezApi\Controller\Component\PipelineComponent;

/**
 * Interface for every Node to run through the endpoint's pipeline.
 *
 * Class Node
 * @package KlezApi\Controller\Node
 */
abstract class Node {
    /**
     * @var PipelineComponent reference to the pipeline.
     */
    private $pipeline;
    /**
     * @var array endpoint's hardcoded config.
     */
    private $config = [];

    /**
     * @param PipelineComponent $pipeline
     */
    public final function initialize(PipelineComponent $pipeline){
        $this->pipeline = $pipeline;
        $this->config = $pipeline->getConfiguration();
    }
    /**
     * Concretize an abstract pipeline and then joins the flow execution into it.
     *
     * @param string $name The pipelines' name denoted by its assoc array key into the abstract pipelines' specification.
     * @return mixed
     */
    public function concretize($name){
        $this->__log();
        $this->log('Concretizing: ' . $name);
        $this->__log();

        return $this->pipeline->concretize($name);
    }

    /**
     * Joins the flow execution into another pipeline
     *
     * @param string $name The pipeline' name denoted by its assoc array key into the endpoint's specification.
     * @return mixed
     */
    public function jump($name){
        $this->__log();
        $this->log('Jumping into: ' . $name);
        $this->__log();

        return $this->pipeline->jump($name);
    }

    /**
     * Halts the current pipeline.
     *
     * @return mixed
     */
    public function halt(){
        $this->__log();
        $this->log('Pipeline Halted!');
        $this->__log();
        return $this->pipeline->halt();
    }

    /**
     * Reads from the config.
     *
     * @param $key
     * @return mixed
     */
    public function config($key){
        return $this->config[$key] ?? null;
    }

    /**
     * Writes into the pipeline's buffer
     *
     * @param $key
     * @param $val
     */
    public final function write($key, $val){
        $this->pipeline->write($key,$val);
    }

    /**
     * Replaces the pipeline's buffer.
     *
     * @param $val
     */
    public final function overwrite($val){
        $this->pipeline->overwrite($val);
    }

    /**
     * Drop a entry from the pipeline's buffer.
     *
     * @param $key
     */
    public final function delete($key){
        $this->pipeline->delete($key);
    }

    /**
     * Reads from the pipeline's buffer.
     *
     * @param null $key
     * @return mixed
     */
    public final function read($key = null){
        return $this->pipeline->read($key);
    }

    /**
     * Reference to the current Request.
     *
     * @return ServerRequest
     */

    public final function request(){
        return $this->pipeline->getController()->request;
    }

    /**
     * Read/Replaces the immutable Response.
     *
     * If $response is null, this function acts as a reader.
     *
     * If $response is not null, this function acts as a writer.
     *
     * @param Response|null $response
     * @return Response
     */

    public final function response(Response $response = null){
        if(is_null($response)){
            return $this->pipeline->getController()->response;
        }
        else{
            $this->pipeline->getController()->response = $response;
        }
    }

    /**
     * Outputs an aesthetic separator
     */
    public function __log(){
        $this->pipeline->__log();
    }

    /**
     * Outputs a log message on the desired level.
     *
     * @param $message
     * @param string $level
     */
    public function log($message, $level='info'){
        $this->pipeline->log($message,$level);
    }

    /**
     * @param $message
     */
    public function info($message){
        $this->pipeline->log($message,'info');
    }

    /**
     * @param $message
     */
    public function warning($message){
        $this->pipeline->log($message,'warning');
    }

    /**
     * @param $message
     */
    public function error($message){
        $this->pipeline->log($message,'error');
    }

    /**
     * @param $message
     */
    public function debug($message){
        $this->pipeline->log($message,'debug');
    }

    /**
     * This function is executed through the pipeline flow.
     *
     * It must implements the Node logic.
     *
     * @return mixed
     */
    abstract function run();
}
